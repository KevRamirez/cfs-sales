 <?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            // Users
            [
                'name' => 'users',
                'display_name' => 'Gestionar Usuarios',
                'module_id' => 1,
            ],
            [
                'name' => 'create_user',
                'display_name' => 'Crear Usuario',
                'module_id' => 1,
            ],
            [
                'name' => 'edit_user',
                'display_name' => 'Editar Usuario',
                'module_id' => 1,
            ],
            [
                'name' => 'delete_user',
                'display_name' => 'Eliminar Usuario',
                'module_id' => 1,
            ],

            // Roles
            [
                'name' => 'roles',
                'display_name' => 'Gestionar Roles',
                'module_id' => 2,
            ],
            [
                'name' => 'create_role',
                'display_name' => 'Crear Rol',
                'module_id' => 2,
            ],
            [
                'name' => 'edit_role',
                'display_name' => 'Editar Rol',
                'module_id' => 2,
            ],
            [
                'name' => 'delete_role',
                'display_name' => 'Eliminar Rol',
                'module_id' => 2,
            ],

            // Permissions
            [
                'name' => 'permissions',
                'display_name' => 'Gestionar Permisos',
                'module_id' => 3,
            ],
            [
                'name' => 'create_permission',
                'display_name' => 'Crear Permiso',
                'module_id' => 3,
            ],
            [
                'name' => 'edit_permission',
                'display_name' => 'Editar Permiso',
                'module_id' => 3,
            ],
            [
                'name' => 'delete_permission',
                'display_name' => 'Eliminar Permiso',
                'module_id' => 3,
            ],
            [
                'name' => 'assign_permission',
                'display_name' => 'Asignar Permiso',
                'module_id' => 3,
            ],
        ];

        $super_admin_role = Role::find(1);
        $gerente_role = Role::find(2);

        foreach ($permissions AS $permission) {
            $permission = Permission::create(['name' => $permission['name'], 'display_name' => $permission['display_name'], 'module_id' => $permission['module_id']]);
            $super_admin_role->givePermissionTo($permission);
        }

    }
}
