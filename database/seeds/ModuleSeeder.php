<?php

use App\Models\Module\ConfigModule;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            'Usuarios',
            'Roles',
            'Permisos',
            'Módulos',
        ];

        foreach ($modules AS $module) {
            ConfigModule::create(['name' => $module]);
        }
    }
}
