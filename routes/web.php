<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    \Auth::logout();
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('/list', 'User\UserController@list')->name('list');
    Route::get('/getAllUsers', 'User\UserController@getAllUsers')->name('getAllUsers');
    Route::get('/profile', 'User\UserController@profile')->name('profile');
});

Route::group(['prefix' => 'family', 'as' => 'family.'], function(){
    Route::get('/', 'Family\FamilyController@view')->name('view');
    Route::get('/getAll', 'Family\FamilyController@getAll')->name('getAll');
    Route::post('/create', 'Family\FamilyController@create')->name('create');
    Route::post('/update', 'Family\FamilyController@update')->name('update');
    Route::get('/delete/{id}', 'Family\FamilyController@delete')->name('delete');
});


Route::group(['prefix' => 'product', 'as' => 'product.'], function (){
    Route::get('/', 'Products\ProductController@view')->name('view');
    Route::get('/getProducts', 'Products\ProductController@getProducts')->name('getProducts');
    Route::get('/getFamilies', 'Products\ProductController@getFamilies')->name('getFamilies');
    Route::post('/create', 'Products\ProductController@create')->name('create');
    Route::post('/update', 'Products\ProductController@update')->name('update');
    Route::get('/delete/{id}', 'Products\ProductController@delete')->name('delete');
});

Route::group(['prefix' => 'provincie', 'as' => 'provincie.'], function (){
    Route::get('/', 'Provincie\ProvincieController@view')->name('view');
    Route::get('/getAll', 'Provincie\ProvincieController@getAll')->name('getAll');
    Route::post('/create', 'Provincie\ProvincieController@create')->name('create');
    Route::post('/update', 'Provincie\ProvincieController@update')->name('update');
    Route::get('/delete/{id}', 'Provincie\ProvincieController@delete')->name('delete');
});

Route::group(['prefix' => 'zone', 'as' => 'zone.'], function (){
    Route::get('/', 'Zone\ZoneController@view')->name('view');
    Route::get('/getAll', 'Zone\ZoneController@getAll')->name('getAll');
    Route::get('/getProvincies', 'Zone\ZoneController@getProvincies')->name('getProvincies');
    Route::post('/create', 'Zone\ZoneController@create')->name('create');
    Route::post('/update', 'Zone\ZoneController@update')->name('update');
    Route::get('/delete/{id}', 'Zone\ZoneController@delete')->name('delete');
});

Route::group(['prefix' => 'community', 'as' => 'community.'], function (){
    Route::get('/', 'Community\CommunityController@view')->name('view');
    Route::get('/getAll', 'Community\CommunityController@getAll')->name('getAll');
    Route::post('/create', 'Community\CommunityController@create')->name('create');
    Route::post('/update', 'Community\CommunityController@update')->name('update');
    Route::get('/delete/{id}', 'Community\CommunityController@delete')->name('delete');
});

Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
    Route::get('/', 'Client\ClientController@view')->name('view');
    Route::get('/getProvincie', 'Client\ClientController@getProvincie')->name('getProvincie');
    Route::get('/getClients', 'Client\ClientController@getClients')->name('getClients');
    Route::post('/create', 'Client\ClientController@saveClient')->name('create');
    Route::post('/updateClient', 'Client\ClientController@updateClient')->name('updateClient');
    Route::get('/delete/{id}', 'Client\ClientController@delete')->name('delete');
});

Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.'], function (){
    Route::post('/uploadShopDiary', 'Dashboard\DashboardController@uploadShopDiary')->name('uploadShopDiary');
    Route::get('/getDairyReport', 'Dashboard\DashboardController@getDairyReport')->name('getDairyReport');
    Route::get('/getGraph/{type}', 'Dashboard\DashboardController@getGraph')->name('getGraph');
});

Route::group(['prefix' => 'reports', 'as' => 'reports.'], function (){
    Route::get('/', 'Reports\ReportsController@view')->name('view');
    Route::post('/filter', 'Reports\ReportsController@filterReport')->name('filter');
    Route::post('/export', 'Reports\ReportsController@exportReport')->name('export');
});

Route::group(['prefix' => 'users', 'as' => 'users.'], function (){ 
    Route::get('/', 'User\UserController@view')->name('view');
    Route::get('/lists', 'User\UserController@lists')->name('lists');
    Route::post('/create', 'User\UserController@createUser')->name('create');
    Route::post('/update', 'User\UserController@updateUser')->name('update');
    Route::post('/delete', 'User\UserController@deleteUser')->name('delete');
    Route::post('/changePassword', 'User\UserController@changePassword')->name('changePassword');
    Route::get('/resetPassword/{idUser}', 'User\UserController@resetPassword')->name('resetPassword');
});
