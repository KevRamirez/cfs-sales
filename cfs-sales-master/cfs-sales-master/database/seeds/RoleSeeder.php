<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Admin',
            'Vendedora',
           
        ];

        foreach ($roles AS $role) {
            $role = Role::create(['name' => $role]);
        }

    }
}
