<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;

class genericExport implements FromArray, WithTitle
{
    use Exportable;

    protected $result;

    public function __construct(array $result)
    {
        $this->result = $result;
    }

    public function array(): array
    {
        return $this->result;
    }

    public function title(): string
    {
        return 'Masters';
    }
}
