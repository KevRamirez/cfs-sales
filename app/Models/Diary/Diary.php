<?php

namespace App\Models\Diary;

use App\Models\Clients\Client;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Diary extends Model
{
    protected $table = "purchases_dairy";

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function client(){
        return $this->belongsTo(Client::class,'client_id','id');
    }
}
