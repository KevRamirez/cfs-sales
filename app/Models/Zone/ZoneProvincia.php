<?php

namespace App\Models\Zone;

use App\Models\Provincie\Provincie;
use Illuminate\Database\Eloquent\Model;

class ZoneProvincia extends Model
{
    protected $table = "zona_provincia";

    public function provincie()
    {
        return $this->belongsTo(Provincie::class,'provincie_id','id');
    }

}
