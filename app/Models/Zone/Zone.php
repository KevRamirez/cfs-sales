<?php

namespace App\Models\Zone;

use App\Models\Provincie\Provincie;
use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = "zona";

    public function provincies()
    {
        return $this->hasMany(ZoneProvincia::class,'zona_id','id');
    }
}
