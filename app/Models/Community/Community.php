<?php

namespace App\Models\Community;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $table = "communities";
}
