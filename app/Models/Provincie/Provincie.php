<?php

namespace App\Models\Provincie;

use Illuminate\Database\Eloquent\Model;

class Provincie extends Model
{
    protected $table = "provincia";
}
