<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserHealthCondition extends Model
{
    protected $table = 'users_health_conditions';
}
