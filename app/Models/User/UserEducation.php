<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserEducation extends Model
{
    protected $table = 'users_education';
}
