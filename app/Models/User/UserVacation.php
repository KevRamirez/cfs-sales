<?php

namespace App\Models\User;

use App\Models\Main\MainVacationStatus;
use Illuminate\Database\Eloquent\Model;

class UserVacation extends Model
{
    protected $table = 'users_vacations';

    public function status()
    {
        return $this->belongsTo(MainVacationStatus::class);
    }
}
