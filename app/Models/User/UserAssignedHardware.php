<?php

namespace App\Models\User;

use App\Models\Main\MainHardwareType;
use Illuminate\Database\Eloquent\Model;

class UserAssignedHardware extends Model
{
    protected $table = 'users_assigned_hardware';

    public function type()
    {
        return $this->belongsTo(MainHardwareType::class, 'type');
    }
}
