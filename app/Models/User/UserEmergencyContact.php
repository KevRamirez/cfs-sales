<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserEmergencyContact extends Model
{
    protected $table = 'users_emergency_contacts';
}
