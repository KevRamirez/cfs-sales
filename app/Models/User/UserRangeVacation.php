<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserRangeVacation extends Model
{
    protected $table = 'users_range_vacations';

    public function vacations()
    {
        return $this->hasMany(UserVacation::class, 'range_id');
    }
}
