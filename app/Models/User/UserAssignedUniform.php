<?php

namespace App\Models\User;

use App\Models\Main\MainUniformType;
use Illuminate\Database\Eloquent\Model;

class UserAssignedUniform extends Model
{
    protected $table = 'users_assigned_uniforms';

    public function type()
    {
        return $this->belongsTo(MainUniformType::class, 'type');
    }
}
