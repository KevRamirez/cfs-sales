<?php

namespace App\Models\User;

use App\Models\Main\MainVehicleType;
use Illuminate\Database\Eloquent\Model;

class UserVehicle extends Model
{
    protected $table = 'users_vehicles';


    public function type()
    {
        return $this->belongsTo(MainVehicleType::class);
    }
}
