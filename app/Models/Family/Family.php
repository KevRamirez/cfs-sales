<?php

namespace App\Models\Family;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $table = "family";
}
