<?php

namespace App\Models\Clients;

use App\Models\Provincie\Provincie;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = "clients";

    public function provincie()
    {
        return $this->belongsTo(Provincie::class,'provincie_id','id');
    }
}
