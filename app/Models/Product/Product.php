<?php

namespace App\Models\Product;

use App\Models\Family\Family;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    public function family()
    {
        return $this->belongsTo(Family::class,'family_id','id');
    }
}
