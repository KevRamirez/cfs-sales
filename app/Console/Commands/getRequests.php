<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Easysoft\Request;
use App\Models\Main\MainPage;
use App\Models\Studio\Studio;
use App\Models\Studio\StudioAccount;
use App\Models\Studio\StudioAccountUser;
use App\Models\Studio\StudioRequest;
use stdClass;
use App\Events\RequestEvent;
use App\Events\TicketEvent;
use App\Http\Controllers\Studio\StudioController;

class getRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getrequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function getIdType($type)
    {
        $id = null;
        switch ($type) {
            case 'cc':
                //CEDULA
                $id = 1;
                break;
            case 'ce':
                //CEDULA EXTRANJERA
                $id = 2;
                break;
            case 'pa':
                //PASAPORTE
                $id = 3;
                break;
            case 'ni':
                //NIT
                $id = 4;
                break;
        }

        return $id;
    }

    private function createdUsers($modeloData)
    {
        $studioAccountUser = new StudioAccountUser;

        $studioAccountUser->document_id = $modeloData->idType;
        $studioAccountUser->studio_id = $modeloData->studioid;
        $studioAccountUser->document_number = $modeloData->document_number;
        $studioAccountUser->image1 = $modeloData->image1;
        $studioAccountUser->image2 = $modeloData->image2;
        $studioAccountUser->image3 = $modeloData->image3;
        $studioAccountUser->image4 = $modeloData->image4;
        $studioAccountUser->password_plataform = $modeloData->password_plataform;

        $names = explode(' ', $modeloData->name);
        $lentghNames = count($names);
        if ($lentghNames == 4) {
            $studioAccountUser->first_name = $names[0];
            $studioAccountUser->second_name = $names[1];
            $studioAccountUser->last_name = $names[2];
            $studioAccountUser->second_last_name = $names[3];
        } else if ($lentghNames == 3) {
            $studioAccountUser->first_name = $names[0];
            $studioAccountUser->last_name = $names[1];
            $studioAccountUser->second_last_name = $names[2];
        } else if ($lentghNames == 2) {
            $studioAccountUser->first_name = $names[0];
            $studioAccountUser->last_name = $names[1];
        } else if ($lentghNames > 4) {
            $studioAccountUser->last_name = $names[$lentghNames - 2];
            $studioAccountUser->second_last_name = $names[$lentghNames - 1];
            $lentghNames = $lentghNames - 2;
            $name = "";
            for ($i = 0; $i < $lentghNames; $i++) {
                $name .= $names[$i] . ' ';
            }
            $studioAccountUser->first_name = $name;
        }
        $studioAccountUser->save();

        return $studioAccountUser->id;
    }

    private function updateDataAccountandUser($request, $modeloAdittionalValue)
    {
        $account = StudioAccount::where('id', $request->studios_account_id)->first();

        $account->photo_profile = $modeloAdittionalValue->photoprofile;

        $userPrimary = StudioAccountUser::where('id', $account->account_user_id)->first();
        $userPrimary->image1 = $modeloAdittionalValue->cedfacefront;
        $userPrimary->image2 = $modeloAdittionalValue->cedfaceback;
        $userPrimary->image3 = $modeloAdittionalValue->cedhandfront;
        $userPrimary->image4 = $modeloAdittionalValue->cedhandback;
        $userPrimary->save();

        if ($account->account_user_second_id) {
            $userSecond = StudioAccountUser::where('id', $account->account_user_second_id)->first();
            $userSecond->image1 = $modeloAdittionalValue->addcedfacefront;
            $userSecond->image2 = $modeloAdittionalValue->addcedfaceback;
            $userSecond->image3 = $modeloAdittionalValue->addcedhandfront;
            $userSecond->image4 = $modeloAdittionalValue->addcedhandback;
            $userPrimary->save();
        }
    }

    private function createdStudio($sede)
    {

        $newStudio = new Studio();
        $newStudio->business_name = $sede->nombre;
        $newStudio->commercial_name = $sede->razonsocial;

        switch ($sede->ciudad_id) {
            case 1:
            case 8:
            case 9:
                //CALI
                $newStudio->city_id = 1009;
                $newStudio->department_id = 24;
                break;
            case 2:
                //MEDELLIN
                $newStudio->city_id = 1;
                $newStudio->department_id = 1;
                break;
            case 3:
                //ARMENIA
                $newStudio->city_id = 16;
                $newStudio->department_id = 1;
                break;
            case 4:
                //MANIZALES
                $newStudio->city_id = 338;
                $newStudio->department_id = 6;
                break;
            case 5:
                //BOGOTA
                $newStudio->city_id = 149;
                $newStudio->department_id = 3;
                break;
            case 6:
                //Dosquebradas
                $newStudio->city_id = 841;
                $newStudio->department_id = 20;
                break;
            case 7:
                //Pereira
                $newStudio->city_id = 837;
                $newStudio->department_id = 20;

                break;
        }

        $newStudio->neighborhood = $sede->barrio;
        $newStudio->address = $sede->direccion;
        $newStudio->email = $sede->emailcontacto;
        $newStudio->id_studio_easysoft = $sede->ID;
        $newStudio->save();

        return $newStudio->id;
    }

    private function createdRequest($idStudio, $idModelo, $idAccount, $value, $modeloValue,$created_at)
    {
        $request = new StudioRequest;

        $request->studio_id = $idStudio;
        $request->studios_account_users_id = $idModelo;
        $request->studios_account_id = $idAccount;
        $request->nicknameres = $value->nicknameres;
        $request->nicknameorig = $value->nicknameorig;
        $request->comments = $value->comments;
        $request->password = $value->password;
        $request->status = $this->setStatusRequest($value->status_id);
        $request->request_easysoft_id = $value->id;
        $request->created_at = $created_at;

        switch ($modeloValue->category_id) {
            case 3:
                $request->category = 'Gay';
                break;
            case 4:
                $request->category = 'Transexual';
                break;
            case 5:
                $request->category = 'Pareja';
                break;
            case 11:
                $request->category = 'Heterosexual';
                break;
        }

        $request->save();
    }

    private function createdAccount($idStudio, $idModelo, $idModeloSecond, $idpage, $modeloAdittionalValue,$password)
    {
        $account = new StudioAccount;
        $account->studio_id = $idStudio;
        $account->account_user_id = $idModelo;
        if ($idModeloSecond) {
            $account->account_user_second_id = $idModeloSecond;
        }
        $account->account_status_id = 1;
        $account->page_id = $idpage;
        $account->nick = "";
        $account->photo_profile = $modeloAdittionalValue->photoprofile;
        if($idpage == 12){ //LiveCamsMates  
            $password = substr($password, 0, -1).'$';
        }
        $account->password_plataform = $password;
        
        $account->save();

        return $account->id;
    }

    private function setStatusRequest($status)
    {
        switch ($status) {
            case 6:
                return 1;
                break;
            case 7:
                return 2;
                break;
            case 8:
                return 3;
                break;
            case 9:
                return 4;
                break;
            case 10:
                return 5;
                break;
            case 11:
                return 6;
                break;
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /*
        6	Registrada	
        7	En proceso	
        8	Detenida	
        9	Solicitud de cambios	
        10	Finalizada
        11	Rechazada	
        */
        $requests = Request::with('modelo')
            ->with('pagina')
            ->with('estado')
            ->where('category_id', 6) //CREAR NICK
            ->whereNotIn('status_id', [9, 10, 11, 19])
            ->orderBy('id', 'desc')
            ->get();

        $studioController = new StudioController();

        foreach ($requests as $value) {

            $modeloValue = $value->modelo;
            $modeloAdittionalValue = $value->modelo->additional;

            $request = StudioRequest::where('request_easysoft_id', $value->id)->first();
            if ($request) {
                if ($request->status != $this->setStatusRequest($value->status_id)) {
                    $request->nicknameres = $value->nicknameres;
                    $request->nicknameorig = $value->nicknameorig;
                    $request->comments = $value->comments;
                    $request->password = $value->password;
                    $request->status = $this->setStatusRequest($value->status_id);
                    $request->save();
                    $this->updateDataAccountandUser($request, $modeloAdittionalValue);
                }
                continue;
            }

            $idStudio = null;
            $studio = Studio::where('id_studio_easysoft', $value->modelo->sede->ID)->first();
            if (!$studio) {
                $idStudio = $this->createdStudio($value->modelo->sede);
            } else {
                $idStudio = $studio->id;
            }
            $this->info('Demo:Cron Create Studio');

            $idType = $this->getIdType($modeloValue->idType);
            if (count(explode("-", $modeloValue->identification)) > 1) {
                //PAREJA    
                $ids = explode("-", $modeloValue->identification);
                $names = explode(" y ", $modeloValue->name);
                $ccPrimary = $ids[0];
                $ccSecond = $ids[1];
                $namePrimary = $names[0];
                $nameSecond = $names[1];
            } else {
                //MODELO
                $ccPrimary = $modeloValue->identification;
                $ccSecond = null;
                $namePrimary = $modeloValue->name;
            }

            $modelo = StudioAccountUser::where('document_number', $ccPrimary)->first();
            $modeloSecond = null;
            $idModeloSecond = null;
            
            if ($ccSecond) {
                $modeloSecond = StudioAccountUser::where('document_number', $ccSecond)->first();
            }

            if (!$modelo) {
                //Se crea modelo principal
                $stdModelo = new stdClass();
                $stdModelo->idType = $idType;
                $stdModelo->document_number = $ccPrimary;
                $stdModelo->name = $namePrimary;
                $stdModelo->email = $modeloValue->email;
                $stdModelo->phone = $modeloValue->phone;
                $stdModelo->additionalPhone = $modeloValue->additionalphone;
                $stdModelo->studioid = $idStudio;
                $stdModelo->image1 = $modeloAdittionalValue->cedfacefront;
                $stdModelo->image2 = $modeloAdittionalValue->cedfaceback;
                $stdModelo->image3 = $modeloAdittionalValue->cedhandfront;
                $stdModelo->image4 = $modeloAdittionalValue->cedhandback;
                $stdModelo->password_plataform = $studioController->createRandomPassword();
                $password_plataform = $stdModelo->password_plataform;

                $idModelo = $this->createdUsers($stdModelo);
            } else {
                $idModelo = $modelo->id;
                $password_plataform = $modelo->password_plataform;
            }

            if ($ccSecond) {
                if (!$modeloSecond) {
                    //Se crea modelo second
                    $stdModelo = new stdClass();
                    $stdModelo->idType = $idType;
                    $stdModelo->document_number = $ccSecond;
                    $stdModelo->name = $nameSecond;
                    $stdModelo->email = $modeloValue->email;
                    $stdModelo->phone = $modeloValue->phone;
                    $stdModelo->additionalPhone = $modeloValue->additionalphone;
                    $stdModelo->studioid = $idStudio;
                    $stdModelo->image1 = $modeloAdittionalValue->addcedfacefront;
                    $stdModelo->image2 = $modeloAdittionalValue->addcedfaceback;
                    $stdModelo->image3 = $modeloAdittionalValue->addcedhandfront;
                    $stdModelo->image4 = $modeloAdittionalValue->addcedhandback;
                    $stdModelo->password_plataform = $studioController->createRandomPassword();

                    $idModeloSecond = $this->createdUsers($stdModelo);
                } else {
                    $idModeloSecond = $modeloSecond->id;
                }
            }

            $this->info('Demo:Cron Create Users');

            $paginas = MainPage::get();
            $idpage = null;
            foreach ($paginas as $page) {
                if (trim(strtoupper($value->pagina->pagina)) == trim(strtoupper($page->name))) {
                    $idpage = $page->id;
                    break;
                }
            }

            $idAccount = $this->createdAccount($idStudio, $idModelo, $idModeloSecond, $idpage, $modeloAdittionalValue,$password_plataform);

            $this->info('Demo:Cron Create Account');

            $this->createdRequest($idStudio, $idModelo, $idAccount, $value, $modeloValue,$value->created_at);

            $this->info('Demo:Cron Create Request');

            Log::info('IdRequest->' . $value);
        }

        broadcast(new RequestEvent(['method' => "getRequests"]));
        $this->info('Demo:Cron Cummand Run successfully!');
    }
}
