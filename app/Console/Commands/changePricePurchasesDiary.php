<?php

namespace App\Console\Commands;

use App\Models\Diary\Diary;
use Illuminate\Console\Command;

class changePricePurchasesDiary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:changePricePurchasesDiary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $dairies = Diary::all();


        foreach ($dairies as $key => $value) {
            $this->info('KEY :: '.$key);

            $value->price_total = $value->price_product * $value->count_product;

            $value->save();
        }
    }
}
