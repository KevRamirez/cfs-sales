<?php

namespace App\Console\Commands;

use App\Models\Studio\StudioAccount;
use Illuminate\Console\Command;

class setStudioAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setStudioAcount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'es para pruebas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $accounts = StudioAccount::get();
        $dateMin = 1612190943000;
        $dateMax = 1618238943000;
        $this->info('Count -> '.count($accounts));
        foreach ($accounts as $key => $value) {
            $this->info('Index ->'.$key);
            $dateMillisecond = mt_rand($dateMin,$dateMax) / 1000;
            $userid = mt_rand(1,3);
            $dateFormat = date("Y-m-d H:i:s",$dateMillisecond);

            $value->approbe_date = $dateFormat;
            $value->created_by = $userid;
            $value->save();
        }
        return 'hola';
    }
}
