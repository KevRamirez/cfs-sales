<?php

namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use App\Models\Module\ConfigModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        // Check permissions
        $this->middleware(['permission:permissions']);
    }

    public function list()
    {
        $roles = [];

        $system_roles = Role::all();
        $modules = ConfigModule::all();

        foreach ($system_roles AS $system_role) {
            $roles[] = [
                'id' => $system_role->id,
                'label' => $system_role->name,
            ];
        }

        return view('modules.permissions.list')->with(['roles' => json_encode($roles), 'modules' => $modules]);
    }

    public function getAllPermissions()
    {
        $data = [];

        $permissions = Permission::with('roles')->get();

        foreach ($permissions As $permission) {
            $roles = [];

            foreach ($permission->roles AS $role) {
                $roles[] = [
                    'id' => $role->id,
                    'label' => $role->name,
                ];
            }

            $data[] = [
                'id' => $permission->id,
                'name' => $permission->name,
                'display_name' => $permission->display_name,
                'module_id' => $permission->module_id,
                'module_name' => ConfigModule::find($permission->module_id)->name,
                'roles' => $roles
            ];
        }

        //dd($permissions);

        return response()->json($data);
    }

    public function setPermissions(Request $request)
    {
        $permission = Permission::findById($request->permission_id);
        DB::delete("DELETE FROM config_role_has_permissions WHERE permission_id = $request->permission_id");

        foreach ($request->roles AS $role) {
            $system_role = Role::findById($role['id']);

            $system_role->givePermissionTo($permission);
        }

        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();

        return response()->json(['ok' => true, 'code' => 0]);
    }

    public function createPermission(Request $request)
    {
        $request->validate(
            [
                'display_name' => "required|unique:config_permissions,display_name|max:255",
                'name' => "required|unique:config_permissions,name|max:255",
                'module_id' => "required",
            ],
            [
                'display_name.required' => "El nombre es obligatorio.",
                'display_name.unique' => "El nombre ya existe.",
                'display_name.max' => "El nombre no puede exceder los 255 caracteres.",
                'name.required' => "El nombre del permiso es obligatorio.",
                'name.unique' => "El nombre del permiso ya existe.",
                'name.max' => "El nombre no puede exceder los 255 caracteres.",
                'module_id.required' => "Debe seleccionar el módulo.",
            ]
        );

        $permission = Permission::create(
            [
                'name' => $request->name,
                'display_name' => $request->display_name,
                'module_id' => $request->module_id
            ]
        );

        // Assign to Super Admin
        $system_role = Role::findById(1);
        $system_role->givePermissionTo($permission);

        return response()->json(['ok' => true]);
    }
}
