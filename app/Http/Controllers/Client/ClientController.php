<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Clients\Client;
use App\Models\Community\Community;
use App\Models\Provincie\Provincie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;

class ClientController extends Controller
{
    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {
        return view('modules.clients.list');
    }

    public function getProvincie()
    {

        return response()->json(['status'=>TRUE,'data'=>Provincie::where('status',1)->get()]);
    }

    public function getClients()
    {

        return response()->json(['status'=>TRUE,'data'=>Client::where('status',1)->with('provincie')->get()]);
    }

    public function saveClient(Request $request)
    {
        $code = $request->code;
        $name = $request->name;
        $estanco = $request->estanco;
        $street = $request->street;
        $population = $request->population;
        $codpos = $request->codpos;
        $phone = $request->phone;
        $cif = $request->cif;
        $sap = $request->sap;
        $wildcard = $request->wildcard;
        $provincie = $request->provincie;

        $clientExist = Client::where('code',$code)->first();
        if($clientExist){
            return response()->json(['status'=>FALSE,'message'=>'El codigo del estanco ya se encuentra registrado']);
        }

        DB::beginTransaction();
        
        try{

            $client = new Client();

            $client->code = $code;
            $client->name = $name;
            $client->estanco = $estanco;
            $client->street = $street;
            $client->population = $population;
            $client->codpos = $codpos;
            $client->phone = $phone;
            $client->cif = $cif;
            $client->sap = $sap;
            $client->wildcard = $wildcard;
            $client->provincie_id = $provincie;

            $client->save();

            DB::commit();

            return response()->json(['status'=>TRUE,'data'=>Client::where('id',$client->id)->with('provincie')->first()]);

        }catch(\Exception $e){
            return response()->json(['status'=>FALSE,'message'=>$e->getMessage()]);
        }
        
    }

    public function updateClient(Request $request)
    {
        $id = $request->id;
        $code = $request->code;
        $name = $request->name;
        $estanco = $request->estanco;
        $street = $request->street;
        $population = $request->population;
        $codpos = $request->codpos;
        $phone = $request->phone;
        $cif = $request->cif;
        $sap = $request->sap;
        $wildcard = $request->wildcard;
        $provincie = $request->provincie;

        $clientExist = Client::where('code',$code)->where('id','!=',$id)->first();
        if($clientExist){
            return response()->json(['status'=>FALSE,'message'=>'El codigo del estanco ya se encuentra registrado']);
        }

        DB::beginTransaction();
        
        try{

            $client = Client::where('id',$id)->first();

            $client->code = $code;
            $client->name = $name;
            $client->estanco = $estanco;
            $client->street = $street;
            $client->population = $population;
            $client->codpos = $codpos;
            $client->phone = $phone;
            $client->cif = $cif;
            $client->sap = $sap;
            $client->wildcard = $wildcard;
            $client->provincie_id = $provincie;

            $client->save();

            DB::commit();

            return response()->json(['status'=>TRUE,'data'=>Client::where('id',$id)->with('provincie')->first()]);

        }catch(\Exception $e){
            return response()->json(['status'=>FALSE,'message'=>$e->getMessage()]);
        }
        
    }

    public function delete($id)
    {
        $client = Client::where('id',$id)->first();

        if(!$client){
            return response()->json(['status'=>FALSE,'message'=>'La zona no se encontró']);
        }

        $client->status = 0;
        $client->save();

        return response()->json(['status'=>TRUE]);
    }
}