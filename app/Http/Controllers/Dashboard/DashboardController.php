<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Imports\DiaryImport;
use App\Models\Diary\Diary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Traits\HasRoles;
use stdClass;

class DashboardController extends Controller
{
    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function getStatistics()
    {
        $data = [];

        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;

        $todayDate = date("Y-m-d", strtotime($today));
        $initDate = date("Y-m-d", strtotime("$year-$month-01"));
        $threeMonths = Carbon::now()->subDays(90)->format('Y-m-d');

        $productCount = Diary::select(DB::raw('sum(count_product) as total'))->where('status',1)->whereDate('date', '>=', $initDate)->first();
        $productCountTotal = Diary::select(DB::raw('sum(count_product) as total'))->whereDate('date', '>=', $initDate)->first();
        $productPopulity = Diary::select(DB::raw('sum(count_product) as total'),'product_id')->with('product')->with('product.family')->where('status',1)->whereDate('date', '>=', $initDate)->groupBy('product_id')->limit(5)->orderBy('total','desc')->get();
        $sumTotalProduct = Diary::select(DB::raw('sum(price_product*count_product) as total'))->where('status',1)->whereDate('date', '>=', $initDate)->first();

        //$productCount = Diary::select(DB::raw('sum(count_product) as total'))->where('status',1)->first();
        //$productCountTotal = Diary::select(DB::raw('sum(count_product) as total'))->first();
        //$productPopulity = Diary::select(DB::raw('sum(count_product) as total'),'product_id')->with('product')->with('product.family')->where('status',1)->groupBy('product_id')->limit(8)->orderBy('total','desc')->get();
        //$sumTotalProduct = Diary::select(DB::raw('sum(price_product*count_product) as total'))->where('status',1)->first();
        $ordersThreeMonths = Diary::select(DB::raw('sum(count_product) as total'),'product_id',DB::raw('sum(count_product/3) as media'))->with('product')->with('product.family')->where('status',1)->whereBetween('date', [$threeMonths, $todayDate])->groupBy('product_id')->limit(10)->orderBy('total','desc')->get();

        return [
            'productCount' => $productCount,
            'productPopulity' => $productPopulity,
            'productTotal' => $sumTotalProduct,
            'productCountTotal' => $productCountTotal,
            'ordersThreeMonths' => $ordersThreeMonths
        ];
    }

    private function serializeGraphDays()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;

        $initDate = date("Y-m-d", strtotime("$year-$month-01"));
        $todayDate = date("Y-m-d", strtotime($today));

        $data = new stdClass();
        $data->data = [];
        $data->dates = [];

        while (strtotime($initDate) <= strtotime($today)) {

            $productCount = Diary::select(DB::raw('sum(count_product) as total'))->where('status',1)->whereBetween('date',["$initDate 00:00:00","$initDate 23:59:59"])->first();

            array_push($data->data, $productCount->total ? $productCount->total : 0);
            array_push($data->dates, $initDate);
            $initDate = date("Y-m-d", strtotime("+1 day", strtotime($initDate)));
        }

        return $data;

    }

    private function serializeGraphMonth()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year  = $today->year;
        $i = 1;

        $data = new stdClass();
        $data->data = [];
        $data->dates = [];
        $data->dateslast = [];

        while($i <= $month){
            $initDate = date("Y-m-d", strtotime("$year-$i-01"));
            $lastDate = date("Y-m-d", strtotime("-1 day",strtotime("+1 month", strtotime($initDate))));

            $productCount = Diary::select(DB::raw('sum(count_product) as total'))->where('status',1)->whereBetween('date',["$initDate 00:00:00","$lastDate 23:59:59"])->first();

            array_push($data->data, $productCount->total ? $productCount->total : 0);
            array_push($data->dates, $initDate);
            array_push($data->dateslast, $lastDate);
            $i++;
        }

        return $data;

    }

    public function uploadShopDiary(Request $request)
    {
        $filename = uniqid() . '_' . time() . '.' . $request->file('file')->getClientOriginalExtension();

        $request->file('file')->storeAs('public/imports',$filename);

        $excelImport = new DiaryImport();

        Excel::import($excelImport,public_path("storage/imports/$filename"));

        $statistics = $this->getStatistics();

        return response()->json(['status'=>TRUE,'data'=>Diary::with('product')->with('product.family')->with('client')->get(),'statistics'=>$statistics]);
        //return $request;
    }

    public function getDairyReport()
    {
        $today = Carbon::now();
        $month = $today->month;
        $year = $today->year;

        $todayDate = date("Y-m-d", strtotime($today));
        $initDate = date("Y-m-d", strtotime("$year-$month-01"));
        $threeMonths = Carbon::now()->subDays(90)->format('Y-m-d');
        $statistics = $this->getStatistics();

        return response()->json(['status'=>TRUE,'data'=>Diary::with('product')->with('product.family')->with('client')->whereBetween('date', [$threeMonths, $todayDate])->get(),'statistics'=>$statistics]);
    }

    public function getGraph($type)
    {
        if($type == 1){
            $graph = $this->serializeGraphDays();
        }else{
            $graph = $this->serializeGraphMonth();
        }

        return response()->json(['status'=>TRUE,'data'=>$graph]);
    }
}
