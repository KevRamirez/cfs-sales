<?php

namespace App\Http\Controllers\reports;

use App\Exports\genericExport;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use stdClass;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {
        return view('modules.reports.view');
    }

    private function reportAnualProvincia($minDate,$maxDate)
    {
        $yearMin = (int) date('Y', strtotime($minDate));
        $yearMax = (int) date('Y', strtotime($maxDate));

        $provincies = DB::select("Select pr.name,pr.id
            from purchases_dairy as pd
            inner join clients as c on c.id = pd.client_id
            inner join provincia as pr ON c.provincie_id = pr.id
            where YEAR(pd.date) BETWEEN $yearMin and $yearMax
            group by pr.id
            order by pr.name;");

        $families = DB::select("Select f.id,f.code
            from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            where YEAR(pd.date) BETWEEN $yearMin and $yearMax
            group by f.id
            order by f.name;");

        $data = [];

        foreach ($provincies as $value) {

            $newYearMin = $yearMin;

            for($i = 1;$newYearMin <= $yearMax;$i++){
                $row = new stdClass();
                $row->provincie_id = $value->id;
                $row->provincie = $value->name;
                $row->date = $newYearMin;

                $provincieforyearfamily = DB::select("
                    Select f.id,sum(pd.price_total) as total
                    from purchases_dairy as pd
                    INNER join products as p on p.id = pd.product_id
                    INNER join family as f on f.id = p.family_id
                    inner join clients as c on c.id = pd.client_id
                    inner join provincia as pr ON c.provincie_id = pr.id
                    where YEAR(pd.date) = $newYearMin and pr.id = $value->id
                    group by f.id;");

                if(count($provincieforyearfamily) > 0){
                    $total = 0;
                    foreach ($provincieforyearfamily as $valueProvincieFamily) {
                        $total += (double) $valueProvincieFamily->total;
                        $row->{'familie'.$valueProvincieFamily->id} = $valueProvincieFamily->total;
                    }
                    $row->total = $total;
                    array_push($data,$row);
                }

                $newYearMin += 1;
            }
        }

        return ['data' => $data,'families' => $families];
    }

    private function reportDiasCFS($minDate,$maxDate)
    {

        $clients = DB::select("
            select pr.id as 'provincie_id',pr.name as 'provincie',c.id as 'cliente_id',c.estanco as 'estanco' from purchases_dairy as pd
            inner join clients as c on c.id = pd.client_id
            inner join provincia as pr ON c.provincie_id = pr.id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by c.id
            order by pr.name;
        ");

        $families = DB::select("Select f.id,f.code
            from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by f.id
            order by f.name;"
        );

        $data = [];

        foreach ($clients as $value) {

            $row = new stdClass();
            $row->minDate = $minDate;
            $row->maxDate = $maxDate;
            $row->provincie_id = $value->provincie_id;
            $row->provincie = $value->provincie;
            $row->cliente_id = $value->cliente_id;
            $row->estanco = $value->estanco;

            $provincieforyearfamily = DB::select("
                Select f.id,sum(pd.price_total) as total
                from purchases_dairy as pd
                INNER join products as p on p.id = pd.product_id
                INNER join family as f on f.id = p.family_id
                where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59' and pd.client_id = $value->cliente_id
                group by f.id;");

            if(count($provincieforyearfamily) > 0){
                $total = 0;
                foreach ($provincieforyearfamily as $valueProvincieFamily) {
                    $total += (double) $valueProvincieFamily->total;
                    $row->{'familie'.$valueProvincieFamily->id} = $valueProvincieFamily->total;
                }
                $row->total = $total;
                array_push($data,$row);
            }
        }

        return ['data' => $data,'families' => $families];
    }

    private function reportAnualClient($minDate,$maxDate)
    {
        $yearMin = (int) date('Y', strtotime($minDate));
        $yearMax = (int) date('Y', strtotime($maxDate));

        $clients = DB::select("
            select pr.id as 'provincie_id',pr.name as 'provincie',c.id as 'cliente_id',c.estanco as 'estanco',p.name as 'product_name',p.id as 'product_id' from purchases_dairy as pd
            inner join clients as c on c.id = pd.client_id
            inner join provincia as pr ON c.provincie_id = pr.id
            inner join products as p ON p.id = pd.product_id
            where YEAR(pd.date) BETWEEN $yearMin and $yearMax
            group by c.id,pd.product_id
            order by pr.name;
        ");

        $data = [];
        $years = [];

        $newYearMin = $yearMin;

        while($newYearMin <= $yearMax){

            array_push($years,$newYearMin);

            $newYearMin += 1;
        }

        foreach ($clients as $value) {

            $row = new stdClass();
            $row->provincie_id = $value->provincie_id;
            $row->provincie = $value->provincie;
            $row->cliente_id = $value->cliente_id;
            $row->estanco = $value->estanco;
            $row->product_name = $value->product_name;

            $provincieforyearfamily = DB::select("
                Select sum(pd.price_total) as total,YEAR(pd.date) as anio
                from purchases_dairy as pd
                where YEAR(pd.date) BETWEEN $yearMin and $yearMax and pd.client_id = $value->cliente_id and pd.product_id = $value->product_id
                group by YEAR(pd.date);");

            if(count($provincieforyearfamily) > 0){
                $total = 0;
                foreach ($provincieforyearfamily as $valueProvincieFamily) {
                    $total += (double) $valueProvincieFamily->total;
                    $row->{'anio'.$valueProvincieFamily->anio} = $valueProvincieFamily->total;
                }
                $row->total = $total;
                array_push($data,$row);
            }
        }

        return ['data' => $data,'years' => $years];

    }

    private function reportDiasProvincia($minDate,$maxDate)
    {

        $provincies = DB::select("Select pr.name,pr.id
            from purchases_dairy as pd
            inner join clients as c on c.id = pd.client_id
            inner join provincia as pr ON c.provincie_id = pr.id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by pr.id
            order by pr.name;");

        $families = DB::select("Select f.id,f.code
            from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by f.id
            order by f.name;");


        $data = [];

        foreach ($provincies as $value) {

            $row = new stdClass();
            $row->minDate = $minDate;
            $row->maxDate = $maxDate;
            $row->provincie_id = $value->id;
            $row->provincie = $value->name;

            $provincieforyearfamily = DB::select("
                Select f.id,sum(pd.price_total) as total
                from purchases_dairy as pd
                INNER join products as p on p.id = pd.product_id
                INNER join family as f on f.id = p.family_id
                inner join clients as c on c.id = pd.client_id
                inner join provincia as pr ON c.provincie_id = pr.id
                where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59' and pr.id = $value->id
                group by f.id;");

            if(count($provincieforyearfamily) > 0){
                $total = 0;
                foreach ($provincieforyearfamily as $valueProvincieFamily) {
                    $total += (double) $valueProvincieFamily->total;
                    $row->{'familie'.$valueProvincieFamily->id} = $valueProvincieFamily->total;
                }
                $row->total = $total;
                array_push($data,$row);
            }
        }

        return ['data' => $data,'families' => $families];
    }

    private function reportDiasVentas($minDate,$maxDate)
    {
        $products = DB::select(
            "Select pd.product_id,sum(pd.count_product) as total_product,pd.price_total,f.code as family_code,p.name as product_name,p.pvp,p.unit_per_box,f.id as familie_id from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            where pd.status = 1 AND
            pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by pd.product_id,pd.price_total
            order by f.code"
        );

        $families = new stdClass();

        $data = [];
        foreach ($products as $key => $value) {

            if(!isset($families->{$value->familie_id})){
                $families->{$value->familie_id} = new stdClass();
                $families->{$value->familie_id}->unidades_vendidas = 0;
                $families->{$value->familie_id}->importe_ventas = 0;
            }

            $row = new stdClass();

            $pvp = (double) $value->pvp;
            $total_product = (int) $value->total_product;

            $row->minDate = $minDate;
            $row->maxDate = $maxDate;
            $row->familie = $value->family_code;
            $row->vitola = $value->product_name;
            $row->box_purchases = $total_product;
            $row->unidades_for_box = $value->unit_per_box;
            $row->unidades_purchases = $total_product * $value->unit_per_box;
            $row->price_total = $value->price_total;
            $row->pvp =  $pvp;
            $row->pvp_total =  (double) $pvp * $row->unidades_purchases;
            $row->familie_id =  $value->familie_id;

            $families->{$value->familie_id}->unidades_vendidas += $row->unidades_purchases;
            $families->{$value->familie_id}->importe_ventas += (double) $row->pvp_total;

            array_push($data,$row);

        }

        return ['data' => $data,'families' => $families];
    }

    private function reportRankingMes($minDate,$maxDate)
    {
        $clients = DB::select("
            select c.id as 'cliente_id',c.estanco as 'estanco' from purchases_dairy as pd
            inner join clients as c on c.id = pd.client_id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by c.id
            order by c.estanco;
        ");

        $families = DB::select("Select f.id,f.code
            from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by f.id
            order by f.name;"
        );

        $data = [];

        foreach ($clients as $value) {

            $row = new stdClass();
            $row->minDate = $minDate;
            $row->maxDate = $maxDate;
            $row->cliente_id = $value->cliente_id;
            $row->estanco = $value->estanco;

            $provincieforyearfamily = DB::select("
                Select f.id,sum(pd.price_total) as total
                from purchases_dairy as pd
                INNER join products as p on p.id = pd.product_id
                INNER join family as f on f.id = p.family_id
                where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59' and pd.client_id = $value->cliente_id
                group by f.id;");

            if(count($provincieforyearfamily) > 0){
                $total = 0;
                foreach ($provincieforyearfamily as $valueProvincieFamily) {
                    $total += (double) $valueProvincieFamily->total;
                    $row->{'familie'.$valueProvincieFamily->id} = $valueProvincieFamily->total;
                }
                $row->total = $total;
                array_push($data,$row);
            }
        }

        return ['data' => $data,'families' => $families];
    }

    private function reportComisionZona($minDate,$maxDate)
    {
        $clients = DB::select("Select  z.name as 'zona_name',c.codpos,pr.name as 'privincia_name',c.estanco,c.id as 'cliente_id'
        from purchases_dairy as pd
        inner join clients as c on c.id = pd.client_id
        inner join provincia as pr ON c.provincie_id = pr.id
        left join zona_provincia as zp ON zp.provincie_id = pr.id
        left join zona as z ON z.id = zp.zona_id
        where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
        group by c.id,z.name
        order by pr.name;");

        $families = DB::select("Select f.id,f.code
            from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by f.id
            order by f.name;"
        );

        $data = [];

        foreach ($clients as $value) {

            $row = new stdClass();
            $row->minDate = $minDate;
            $row->maxDate = $maxDate;
            $row->cliente_id = $value->cliente_id;
            $row->zona_name = $value->zona_name;
            $row->codpos = $value->codpos;
            $row->privincia_name = $value->privincia_name;
            $row->estanco = $value->estanco;

            $provincieforyearfamily = DB::select("
                Select f.id,sum(pd.price_total) as total
                from purchases_dairy as pd
                INNER join products as p on p.id = pd.product_id
                INNER join family as f on f.id = p.family_id
                where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59' and pd.client_id = $value->cliente_id
                group by f.id;");

            if(count($provincieforyearfamily) > 0){
                $total = 0;
                foreach ($provincieforyearfamily as $valueProvincieFamily) {
                    $total += (double) $valueProvincieFamily->total;
                    $row->{'familie'.$valueProvincieFamily->id} = $valueProvincieFamily->total;
                }
                $row->total = $total;
                array_push($data,$row);
            }
        }

        return ['data' => $data,'families' => $families];
    }

    private function reportDiasAcumuladas($minDate,$maxDate)
    {
        $products = DB::select(
            "Select
                p.code as 'codeproduct',
                f.code as 'codefamilia',
                f.name as 'familianame',
                p.name as 'productname',
                p.pvp,
                p.unit_per_box,
                cast(pd.date as date) as date_purchases,
                sum(pd.count_product) as 'count_product',
                c.code as 'codeestanco',
                c.estanco as 'estanco',
                c.name as 'name',
                c.street as 'calle',
                c.population,
                c.codpos,
                pv.name as 'provincia'
            from purchases_dairy as pd
            INNER join products as p on p.id = pd.product_id
            INNER join family as f on f.id = p.family_id
            INNER JOIN clients as c on pd.client_id = c.id
            INNER JOIN provincia as pv on pv.id = c.provincie_id
            where pd.date BETWEEN '$minDate 00:00:00' AND '$maxDate 23:59:59'
            group by p.id,cast(pd.date as date),c.id
            order by f.code"
        );

        $data = [];
        foreach ($products as $key => $value) {

            $row = new stdClass();

            $pvp = (double) $value->pvp;
            $unit = $value->unit_per_box;

            $row->codeproduct = $value->codeproduct;
            $row->codefamilia = $value->codefamilia;
            $row->familianame = $value->familianame;
            $row->productname = $value->productname;
            $row->pvp = $pvp;
            $row->unit = $unit;
            $row->pvpcaja = $pvp * $unit;
            $row->date = $value->date_purchases;
            $row->count_product = $value->count_product;
            $row->unit_purchases = (double) $value->count_product * $unit;
            $row->importventa = (double) $row->unit_purchases * $pvp;

            $row->codeestanco = $value->codeestanco;
            $row->estanco = $value->estanco;
            $row->name = $value->name;
            $row->calle = $value->calle;
            $row->population = $value->population;
            $row->provincia =  $value->provincia;
            array_push($data,$row);
        }

        return ['data' => $data];
    }

    private function reportexistsday()
    {

        $products = DB::select(
            "Select
                p.code as 'codeproduct',
                f.code as 'codefamilia',
                p.name as 'productname',
                p.stock,
                p.unit_per_box,
                p.pvp
            from products as p
            INNER join family as f on f.id = p.family_id
            group by p.id
            order by f.code"
        );

        $data = [];
        foreach ($products as $key => $value) {

            $row = new stdClass();

            $pvp = (double) $value->pvp;
            $unit = $value->unit_per_box;
            $stock = $value->stock;

            $row->codeproduct = $value->codeproduct;
            $row->codefamilia = $value->codefamilia;
            $row->productname = $value->productname;
            $row->stock = $stock;
            $row->unit = $unit;
            $row->stockunit = $stock * $unit;
            $row->pvp = $pvp;
            $row->total = $pvp * $row->stockunit;
            array_push($data,$row);
        }

        return ['data' => $data];
    }

    private function reportrankinganual()
    {

        $years = DB::select("
            select
                YEAR(date) as anio
            from purchases_dairy as pd
            where YEAR(date) IS NOT NULL
            group by YEAR(date)
            order by YEAR(date) DESC
        ");

        $clients = DB::select("
            select
                client_id
            from purchases_dairy as pd
            where status = 1 AND client_id IS NOT NULL
            group by client_id
            order by client_id
        ");

        $data = [];
        $yearHeader = [];
        $clientid = '';
        $venta = '';
        array_push($yearHeader,$years);
        foreach($clients as $client){
            $row = new stdClass();
            $row->total = 0;
            $clientid = $client->client_id;
            /* foreach($years as $year){
                array_push($yearHeader,$year->anio);
                $ventas = DB::select("
                    select
                        sum(pd.price_total) as total,
                        c.estanco
                    from purchases_dairy as pd
                    inner join clients as c on c.id = pd.client_id
                    where YEAR(pd.date) = ".$year->anio." AND
                    client_id = ".$client->client_id."
                    group by pd.client_id
                ");

                $row->estanco = $ventas[0]->estanco;
                $row->total += $ventas[0]->total;
                $row->{'year_'.$year->anio} = $ventas[0]->total;
            } */

            $ventas = DB::select("

            SELECT client_id,price_product,count_product,clients.estanco, price_total AS total,YEAR(date) AS anio
            FROM purchases_dairy
            INNER JOIN clients
            ON clients.id = purchases_dairy.client_id
            WHERE purchases_dairy.client_id = $clientid
            AND purchases_dairy.STATUS = 1
            AND YEAR(purchases_dairy.date) IS NOT NULL
            GROUP BY YEAR(purchases_dairy.date)
            ORDER BY YEAR(purchases_dairy.date) DESC



            ");
            foreach($ventas as $venta){
                $row->{'year_'.$venta->anio} = $venta->total;
                $row->total += round($venta->total, 2);

            }
            $row->estanco = $ventas[0]->estanco;
            $row->clienteid = $ventas[0]->client_id;


            array_push($data,$row);
        }


        return ['data' => $data, 'years' => $years];
    }

    public function filterReport(Request $request)
    {
        $typeInfo = $request->infoSelected;
        $minDate = $request->minDate;
        $maxDate = $request->maxDate;

        $data = null;
        try{
            switch($typeInfo){
                case 1:
                    $data = $this->reportAnualProvincia($minDate,$maxDate);
                    break;
                case 2:
                    $data = $this->reportDiasCFS($minDate,$maxDate);
                    break;
                case 3:
                    $data = $this->reportAnualClient($minDate,$maxDate);
                    break;
                case 4:
                    $data = $this->reportDiasProvincia($minDate,$maxDate);
                    break;
                case 5:
                    $data = $this->reportDiasVentas($minDate,$maxDate);
                    break;
                case 6:
                    $data = $this->reportRankingMes($minDate,$maxDate);
                    break;
                case 7:
                    $data = $this->reportComisionZona($minDate,$maxDate);
                    break;
                case 8:
                    $data = $this->reportDiasAcumuladas($minDate,$maxDate);
                    break;
                case 9:
                    $data = $this->reportexistsday();
                    break;
                case 10:
                    $data = $this->reportrankinganual();
                    break;
            }
            return response()->json(['ok'=>TRUE,'data'=>$data]);
        }catch(\Exception $e){
            return response()->json([
                'ok'=>FALSE,
                'message'=>$e->getMessage(),
                'line'=>$e->getLine()
            ]);
        }
    }

    public function exportReport(Request $request)
    {
        $typeInfo = $request->infoSelected;
        $minDate = $request->minDate;
        $maxDate = $request->maxDate;

        try{
            switch($typeInfo){
                case 1:
                    $data = $this->reportAnualProvincia($minDate,$maxDate);
                    $header = [
                        'Cod Provincia',
                        'Provincia',
                        'Año',
                        'Total',
                    ];

                    $rows = [];

                    foreach ($data['families'] as $value) {
                        array_push($header,$value->code);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->provincie_id,
                            $value->provincie,
                            $value->date,
                            $value->total,
                        ];
                        foreach ($data['families'] as $familie) {
                            array_push($info,isset($value->{'familie'.$familie->id}) ? $value->{'familie'.$familie->id} : '');
                        };

                        array_push($rows,$info);
                    }

                    break;
                case 2:
                    $data = $this->reportDiasCFS($minDate,$maxDate);

                    $header = [
                        'Fecha inicial',
                        'Fecha final',
                        'Cod provincia',
                        'Provincia',
                        'Estanco',
                        'Total',
                    ];

                    $rows = [];

                    foreach ($data['families'] as $value) {
                        array_push($header,$value->code);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->minDate,
                            $value->maxDate,
                            $value->provincie_id,
                            $value->provincie,
                            $value->estanco,
                            $value->total,
                        ];
                        foreach ($data['families'] as $familie) {
                            array_push($info,isset($value->{'familie'.$familie->id}) ? $value->{'familie'.$familie->id} : '');
                        };

                        array_push($rows,$info);
                    }
                    break;
                case 3:
                    $data = $this->reportAnualClient($minDate,$maxDate);
                    $header = [
                        'Cod provincia',
                        'Provincia',
                        'Estanco',
                        'Vitola',
                        'Total',
                    ];

                    $rows = [];

                    foreach ($data['years'] as $value) {
                        array_push($header,$value);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {
                        $info = [
                            $value->provincie_id,
                            $value->provincie,
                            $value->estanco,
                            $value->product_name,
                            $value->total,
                        ];
                        foreach ($data['years'] as $year) {
                            array_push($info,isset($value->{'anio'.$year}) ? $value->{'anio'.$year} : '');
                        };

                        array_push($rows,$info);
                    }

                    break;
                case 4:
                    $data = $this->reportDiasProvincia($minDate,$maxDate);

                    $header = [
                        'Fecha inicial',
                        'Fecha final',
                        'Cod provincia',
                        'Provincia',
                        'Estanco',
                        'Total',
                    ];

                    $rows = [];

                    foreach ($data['families'] as $value) {
                        array_push($header,$value->code);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->minDate,
                            $value->maxDate,
                            $value->provincie_id,
                            $value->provincie,
                            $value->total,
                        ];
                        foreach ($data['families'] as $familie) {
                            array_push($info,isset($value->{'familie'.$familie->id}) ? $value->{'familie'.$familie->id} : '');
                        };

                        array_push($rows,$info);
                    }
                    break;
                case 5:
                    $data = $this->reportDiasVentas($minDate,$maxDate);

                    $header = [
                        'Dia inicial',
                        'Dia final',
                        'COD Familia',
                        'Vitola',
                        'Cajas vendidas',
                        'Unidad por caja',
                        'Unidades vendidas',
                        'Unidades vendidas por marca',
                        'pvp',
                        'Importe venta marca'
                    ];

                    $rows = [];
                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->minDate,
                            $value->maxDate,
                            $value->familie,
                            $value->vitola,
                            $value->box_purchases,
                            $value->unidades_for_box,
                            $value->unidades_purchases,
                            isset($data['families']->{$value->familie_id}) ? $data['families']->{$value->familie_id}->unidades_vendidas : '',
                            $value->pvp,
                            $value->pvp_total,
                            isset($data['families']->{$value->familie_id}) ? $data['families']->{$value->familie_id}->importe_ventas : '',
                        ];

                        array_push($rows,$info);
                    }

                    break;
                case 6:
                    $data = $this->reportRankingMes($minDate,$maxDate);

                    $header = [
                        'Fecha inicial',
                        'Fecha final',
                        'Estanco',
                        'Total',
                    ];

                    $rows = [];

                    foreach ($data['families'] as $value) {
                        array_push($header,$value->code);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->minDate,
                            $value->maxDate,
                            $value->estanco,
                            $value->total,
                        ];
                        foreach ($data['families'] as $familie) {
                            array_push($info,isset($value->{'familie'.$familie->id}) ? $value->{'familie'.$familie->id} : '');
                        };

                        array_push($rows,$info);
                    }
                    break;

                case 7:
                    $data = $this->reportComisionZona($minDate,$maxDate);

                    $header = [
                        'Fecha inicial',
                        'Fecha final',
                        'Zona',
                        'Codpos',
                        'Provincia',
                        'Estanco',
                        'Total',
                    ];

                    $rows = [];

                    foreach ($data['families'] as $value) {
                        array_push($header,$value->code);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->minDate,
                            $value->maxDate,
                            $value->zona_name,
                            $value->codpos,
                            $value->privincia_name,
                            $value->estanco,
                            $value->total,
                        ];
                        foreach ($data['families'] as $familie) {
                            array_push($info,isset($value->{'familie'.$familie->id}) ? $value->{'familie'.$familie->id} : '');
                        };

                        array_push($rows,$info);
                    }
                    break;
                case 8:
                    $data = $this->reportDiasAcumuladas($minDate,$maxDate);

                    $header = [
                        'Codigo',
                        'Familia',
                        'DesFamilia',
                        'vitola',
                        'pvp',
                        'UniCaja',
                        'PVPcaja',
                        'Fecha',
                        'Cajas vendidas',
                        'UniVen',
                        'ImpVenta',
                        'CODEstan',
                        'DesEstan',
                        'Contac',
                        'Calle',
                        'Población',
                        'Provincia'
                    ];

                    $rows = [];

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->codeproduct,
                            $value->codefamilia,
                            $value->familianame,
                            $value->productname,
                            $value->pvp,
                            $value->unit,
                            $value->pvpcaja,
                            $value->date,
                            $value->count_product,
                            $value->unit_purchases,
                            $value->importventa,
                            $value->codeestanco,
                            $value->estanco,
                            $value->name,
                            $value->calle,
                            $value->population,
                            $value->provincia
                        ];

                        array_push($rows,$info);
                    }
                    break;
                case 9:
                    $data = $this->reportexistsday();

                    $header = [
                        'Codigo',
                        'Familia',
                        'vitola',
                        'Cajas',
                        'Unidades',
                        'UniCaja',
                        'pvp',
                        'Total',
                        'Fecha de existencia'
                    ];

                    $rows = [];

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->codeproduct,
                            $value->codefamilia,
                            $value->productname,
                            $value->stock,
                            $value->stockunit,
                            $value->unit,
                            $value->pvp,
                            $value->total,
                            date('d-M')
                        ];

                        array_push($rows,$info);
                    }
                    break;
                case 10:
                    $data = $this->reportrankinganual();

                    $header = [
                        'Codigo',
                        'total'
                    ];

                    $rows = [];

                    foreach ($data['years'] as $value) {
                        array_push($header,$value->anio);
                    }

                    array_push($rows,$header);

                    foreach ($data['data'] as $value) {

                        $info = [
                            $value->estanco,
                            $value->total
                        ];
                        foreach ($data['years'] as $year) {
                            array_push($info,isset($value->{'year_'.$year->anio}) ? $value->{'year_'.$year->anio} : '');
                        };

                        array_push($rows,$info);
                    }

                    break;

            }
            return Excel::download(new genericExport($rows), 'Lista de modelos.xlsx');

        }catch(\Exception $e){
            return response()->json([
                'ok'=>FALSE,
                'message'=>$e->getMessage(),
                'line'=>$e->getLine()
            ]);
        }



    }
}
