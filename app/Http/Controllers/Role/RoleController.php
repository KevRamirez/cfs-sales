<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        // Check permissions
        $this->middleware(['permission:roles']);
    }

    public function list()
    {
        $roles = [];

        $system_roles = Role::all();
        return view('modules.roles.list')->with(['roles' => $system_roles]);
    }

    public function createRole(Request $request)
    {
        $request->validate(
            [
                'name' => "required|unique:config_roles,name|max:255"
            ],
            [
                'name.required' => "El nombre es obligatorio.",
                'name.unique' => "El nombre ya existe.",
                'name.max' => "El nombre no puede exceder los 255 caracteres.",
            ]
        );

        $role = new Role();
        $role->name = $request->name;
        $ok = $role->save();

        return response()->json(['ok' => $ok]);
    }

    public function editRole(Request $request)
    {
        $request->validate(
            [
                'name' => "required|unique:config_roles,name,$request->id|max:255"
            ],
            [
                'name.required' => "El nombre es obligatorio.",
                'name.unique' => "El nombre ya existe.",
                'name.max' => "El nombre no puede exceder los 255 caracteres.",
            ]
        );

        $role = Role::findById($request->id);
        $role->name = $request->name;
        $ok = $role->save();

        return response()->json(['ok' => $ok]);
    }

    public function deleteRole(Request $request)
    {
        try {
            DB::beginTransaction();

            $ok = Role::find($request->id)->delete();

            DB::commit();

            return response()->json(['ok' => $ok]);
        } catch (\Exception $exception) {
            DB::rollback();

            $error_info = [];

            switch ($exception->getCode()) {
                case 23000:
                    $error_info = [
                        'ok' => false,
                        'code' => (int)$exception->getCode(),
                        'msg' => $exception->getMessage(),
                    ];
                    break;

                default:

                    break;
            }

            return response()->json($error_info);
        }
    }

    public function getAllRoles()
    {
        $roles = Role::all();
        return response()->json($roles);
    }
}
