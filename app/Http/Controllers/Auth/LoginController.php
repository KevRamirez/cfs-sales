<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials()
    {
        return [
            'email' => request()->email,
            'password' => request()->password,
            'active' => 1
        ];
    }

    public function redirectTo()
    {
        $user_type = '';
        $user_studio_id = auth()->user()->studio_id;
        $user_role_id = auth()->user()->role_id;

        User::where('id', auth()->user()->id)->update([
            'last_login_at' => Carbon::now()
        ]);

        if ($user_studio_id == 1 && $user_role_id == 1)
        {
            return route('admin');
        }
        else
        {
            return route('home');
        }
    }
}
