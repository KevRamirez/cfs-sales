<?php

namespace App\Http\Controllers\Provincie;

use App\Http\Controllers\Controller;
use App\Models\Provincie\Provincie;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;

class ProvincieController extends Controller
{
    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function view()
    {
        return view('modules.provincie.list');
    }

    public function getAll()
    {
        return response()->json(['status'=>TRUE,'data'=>Provincie::where('status',1)->get()]);
    }

    public function create(Request $request)
    {
        $name = $request->name;
        $community = $request->community;

        $product = new Provincie();
        $product->name = $name;
        $product->community_id = $community;
        $product->save();

        return response()->json(['status'=>TRUE,'data'=>Provincie::where('id',$product->id)->first()]);
    }

    public function update(Request $request)
    {
        $idProvincie = $request->idProvincie;
        $name = $request->name;
        $community = $request->community;

        $provincie = Provincie::where('id',$idProvincie)->first();

        if(!$provincie){
            return response()->json(['status'=>FALSE,'message'=>'Ocurrio un error buscando la familia seleccionada']);
        }

        $provincie->name = $name;
        $provincie->community_id = $community;
        $provincie->save();

        return response()->json(['status'=>TRUE,'data'=>$provincie]);
    }

    public function delete($id)
    {
        $provincie = Provincie::where('id',$id)->first();

        if(!$provincie){
            return response()->json(['status'=>FALSE,'message'=>'el producto no se encontró']);
        }

        $provincie->status = 0;
        $provincie->save();

        return response()->json(['status'=>TRUE]);
    }
}
