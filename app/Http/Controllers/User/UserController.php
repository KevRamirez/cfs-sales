<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Cafeteria\CafeteriaOrder;
use App\Models\Fondelab\FondelabUserinfo;
use App\Models\Main\MainARLEntity;
use App\Models\Main\MainBloodType;
use App\Models\Main\MainCities;
use App\Models\Main\MainCompany;
use App\Models\Main\MainCompanyDepartment;
use App\Models\Main\MainCompensationEntity;
use App\Models\Main\MainContractType;
use App\Models\Main\MainDepartment;
use App\Models\Main\MainDocument;
use App\Models\Main\MainEducationDegree;
use App\Models\Main\MainEPSEntity;
use App\Models\Main\MainHardwareType;
use App\Models\Main\MainLayoffEntity;
use App\Models\Main\MainMaritalStatus;
use App\Models\Main\MainTurn;
use App\Models\Main\MainUniformType;
use App\Models\Main\MainVehicleType;
use App\Models\Studio\Studio;
use App\Models\Studio\StudioDepartment;
use App\Models\Studio\StudioLocation;
use App\Models\User\UserAssignedHardware;
use App\Models\User\UserAssignedUniform;
use App\Models\User\UserEducation;
use App\Models\User\UserEmergencyContact;
use App\Models\User\UserHealthCondition;
use App\Models\User\UserRangeVacation;
use App\Models\User\UserVacation;
use App\Models\User\UserVehicle;
use App\Models\Zone\Zone;
use App\Models\Zone\ZoneProvincia;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        // Check permissions to controller
        //$this->middleware(['permission:users']);
    }

    public function view()
    {
        return view('modules.users.list');
    }

    public function lists()
    {
        $users = User::with('zone')->where('delete',0)->get();
        $zones = Zone::select('id as value','name as text')->get();
        return response()->json(['ok'=>TRUE,'data'=>['users' => $users, 'zones' => $zones]]); 
    }


    public function createUser(Request $request)
    {

        $user = User::where('email',$request->email)->first();

        if($user){
            return response()->json(['status'=>FALSE,'message'=>'El correo ya se encuentra registrado']);
        }
        
        try {
            DB::beginTransaction();

            $user = new User();
            $user->first_name = trim($request->name);
            $user->last_name = trim($request->last_name);
            $user->email = trim($request->email);
            $user->password = Hash::make(123456);
            $user->zone_id = $request->zone;
            $user->phone = $request->phone;

            $user->assignRole(2);

            $user->save();
            DB::commit();

            return response()->json(['status' => TRUE,'data' => User::where('id',$user->id)->with('zone')->first()]);
        } catch (\Exception $exception) {
            DB::rollback();

            return response()->json([
                'status' => FALSE,
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
            ]);
        }
    }

    public function updateUser(Request $request)
    {
        $user = User::where('email',$request->email)->where('id','<>',$request->id)->first();

        if($user){
            return response()->json(['status'=>FALSE,'message'=>'El correo ya se encuentra registrado']);
        }

        try {
            DB::beginTransaction();

            $user = User::find($request->id);
            $user->first_name = trim($request->name);
            $user->last_name = trim($request->last_name);
            $user->email = trim($request->email);
            $user->zone_id = $request->zone;
            $user->phone = $request->phone;

            $user->save();

            DB::commit();

            return response()->json(['status' => TRUE,'data' => User::where('id',$user->id)->with('zone')->first()]);
        } catch (\Exception $exception) {
            DB::rollback();

            return response()->json([
                'status' => FALSE,
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
            ]);
        }
    }

    public function deleteUser(Request $request)
    {
        if(auth()->user()->id == $request->id) {
            $error_info = [
                'status' => false,
                'message' => '¡No puedes borrar tu propio usuario!',
            ];

            return response()->json($error_info);
        }

        try {
            DB::beginTransaction();

            $user = User::find($request->id);
            $user->delete = 1;
            $ok = $user->save();

            DB::commit();

            return response()->json(['status' => $ok]);
        } catch (\Exception $exception) {
            DB::rollback();

            return response()->json([
                'status' => FALSE,
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
            ]);
        }
    }

    public function changePassword(Request $request)
    {
        if (!Hash::check($request->current_pass, Auth::user()->password)) {
            return response()->json(
                [
                    'status' => FALSE,
                    'message' => 'La contraseña ingresada no coincide con la actual'
                    
                ]
            );
        }

        $request->validate(
            [
                'new_pass' => "required|min:6",
            ],
            [
                'new_pass.required' => "Este campo es obligatorio.",
                'new_pass.min' => "La contraseña debe tener al menos 6 caracteres",
            ]
        );

        try {
            DB::beginTransaction();

            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->new_pass);

            if ($request->new_pass != '123456') {
                $user->password_change = 0;
            }

            $ok = $user->save();

            DB::commit();

            return response()->json(['ok' => $ok]);
        } catch (\Exception $exception) {
            DB::rollback();

            return response()->json([
                'status' => FALSE,
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
            ]);
        }
    }

    public function resetPassword($idUser){

        $user = User::where('id',$idUser)->first();

        if(!$user){
            return response()->json(['status'=>FALSE,'message'=>'El usuario no se encuentra registrado']);
        }

        $user->password = Hash::make('123456');
        $user->password_change = 1;
        $user->save();

        return response()->json(['status'=>TRUE]);
    }
}
