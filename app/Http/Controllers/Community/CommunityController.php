<?php

namespace App\Http\Controllers\Community;

use App\Http\Controllers\Controller;
use App\Models\Community\Community;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;

class CommunityController extends Controller
{
    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function view()
    {
        return view('modules.community.list');
    }

    public function getAll()
    {
        return response()->json(['status'=>TRUE,'data'=> Community::where('status',1)->get()]);
    }

    public function create(Request $request)
    {
        $name = $request->name;

        $community = new Community();
        $community->name = $name;
        $community->save();

        return response()->json(['status'=>TRUE,'data'=>Community::where('id',$community->id)->first()]);
    }

    public function update(Request $request)
    {
        $name = $request->name;
        $idcommunity = $request->idCommunity;

        $community = Community::where('id',$idcommunity)->first();

        if(!$community){
            return response()->json(['status'=>FALSE,'message'=>'Ocurrio un error encontrado la comunidad solicitada']);
        }

        $community->name = $name;
        $community->save();

        return response()->json(['status'=>TRUE,'data'=>Community::where('id',$community->id)->first()]);
    }

    public function delete($id)
    {
        $community = Community::where('id',$id)->first();

        if(!$community){
            return response()->json(['status'=>FALSE,'message'=>'La zona no se encontró']);
        }

        $community->status = 0;
        $community->save();

        return response()->json(['status'=>TRUE]);
    }

}
