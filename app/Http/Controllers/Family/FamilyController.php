<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use App\Models\Family\Family;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Double;
use Spatie\Permission\Traits\HasRoles;

class FamilyController extends Controller
{
    

    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function view()
    {
        return view('modules.family.list');
    }

    public function getAll()
    {
        return response()->json(['status'=>TRUE,'data'=>Family::where('status',1)->get()]);
    }

    public function create(Request $request)
    {
        $name = $request->name;
        $code = $request->code;
        $price = $request->price;

        $family = new Family();
        $family->name = $name;
        $family->code = $code;
        $family->price = $price;
        $family->save();

        return response()->json(['status'=>TRUE,'data'=>$family]);
    }


    public function update(Request $request)
    {
        $idFamily = $request->idFamily;
        $name = $request->name;
        $code = $request->code;
        $price = $request->price;

        $family = Family::where('id',$idFamily)->first();

        if(!$family){
            return response()->json(['status'=>FALSE,'message'=>'Ocurrio un error buscando la familia seleccionada']);
        }

        $family->name = $name;
        $family->code = $code;
        $family->price = $price;
        $family->save();

        return response()->json(['status'=>TRUE,'data'=>$family]);
    }

    public function delete($id)
    {
        $family = Family::where('id',$id)->first();

        if(!$family){
            return response()->json(['status'=>FALSE,'message'=>'La familia no se encontró']);
        }

        $family->status = 0;
        $family->save();

        return response()->json(['status'=>TRUE]);
    }

}
