<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Family\Family;
use App\Models\Product\Product;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;

class ProductController extends Controller
{
    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function view()
    {
        return view('modules.products.list');
    }

    public function getFamilies()
    {
        return response()->json(['status'=>TRUE,'data'=>Family::select('id','name')->where('status',1)->get()]);
    }

    public function getProducts()
    {
        return response()->json(['status'=>TRUE,'data'=>Product::where('status',1)->with('family')->get()]);
    }

    public function create(Request $request)
    {
        $name = $request->name;
        $family = $request->family;
        $code = $request->code;
        $pvp = $request->pvp;
        $price = $request->price;
        $unit_per_box = $request->unit_per_box;
        $stock = $request->stock;

        $product = new Product();
        $product->name = $name;
        $product->family_id = $family;
        $product->code = $code;
        $product->pvp = $pvp;
        $product->price_box = $price;
        $product->unit_per_box = $unit_per_box;
        $product->stock = $stock;
        $product->save();

        return response()->json(['status'=>TRUE,'data'=>Product::where('id',$product->id)->with('family')->first()]);
    }

    public function update(Request $request)
    {
        $name = $request->name;
        $family = $request->family;
        $idProduct = $request->idProduct;
        $code = $request->code;
        $pvp = $request->pvp;
        $price = $request->price;
        $unit_per_box = $request->unit_per_box;
        $stock = $request->stock;

        $product = Product::where('id',$idProduct)->first();

        if(!$product){
            return response()->json(['status'=>FALSE,'message'=>'Ocurrio un error encontrado el producto solicitado']);
        }

        $product->name = $name;
        $product->family_id = $family;
        $product->code = $code;
        $product->pvp = $pvp;
        $product->price_box = $price;
        $product->unit_per_box = $unit_per_box;
        $product->stock = $stock;
        $product->save();

        return response()->json(['status'=>TRUE,'data'=>Product::where('id',$product->id)->with('family')->first()]);
    }

    public function delete($id)
    {
        $product = Product::where('id',$id)->first();

        if(!$product){
            return response()->json(['status'=>FALSE,'message'=>'el producto no se encontró']);
        }

        $product->status = 0;
        $product->save();

        return response()->json(['status'=>TRUE]);
    }
}
