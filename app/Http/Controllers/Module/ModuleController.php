<?php

namespace App\Http\Controllers\Module;

use App\Http\Controllers\Controller;
use App\Models\Module\ConfigModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        // Check permissions
        $this->middleware(['permission:modules']);
    }

    public function list()
    {
        return view('modules.modules.list');
    }

    public function getAllModules()
    {
        $modules = ConfigModule::all();
        return response()->json($modules);
    }

    public function create(Request $request)
    {
        $request->validate(
            [
                'name' => "required|unique:config_roles,name|max:255",
            ],
            [
                'name.required' => "Este campo es obligatorio.",
                'name.unique' => "El rol ya existe.",
            ]
        );

        try {
            DB::beginTransaction();

            $module = new ConfigModule();
            $module->name = trim($request->name);

            $ok = $module->save();

            DB::commit();

            return response()->json(['ok' => $ok]);
        } catch (\Exception $exception) {
            DB::rollback();

            $error_info = [
                'ok' => false,
                'code' => (int)$exception->getCode(),
                'msg' => $exception->getMessage(),
            ];

            return response()->json($error_info);
        }
    }

    public function edit(Request $request)
    {
        $request->validate(
            [
                'name' => "required|unique:config_modules,name,$request->id|max:255",
            ],
            [
                'name.required' => "Este campo es obligatorio.",
                'name.unique' => "El módulo ya existe.",
            ]
        );

        try {
            DB::beginTransaction();

            $module = ConfigModule::find($request->id);
            $module->name = trim($request->name);

            $ok = $module->save();

            DB::commit();

            return response()->json(['ok' => $ok]);
        } catch (\Exception $exception) {
            DB::rollback();

            $error_info = [
                'ok' => false,
                'code' => (int)$exception->getCode(),
                'msg' => $exception->getMessage(),
            ];

            return response()->json($error_info);
        }
    }

    public function delete(Request $request)
    {
        try {
            DB::beginTransaction();

            $ok = ConfigModule::find($request->id)->delete();

            DB::commit();

            return response()->json(['ok' => $ok]);
        } catch (\Exception $exception) {
            DB::rollback();

            $error_info = [
                'ok' => false,
                'code' => (int)$exception->getCode(),
                'msg' => $exception->getMessage(),
            ];

            return response()->json($error_info);
        }
    }
}
