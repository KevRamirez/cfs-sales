<?php

namespace App\Http\Controllers\Zone;

use App\Http\Controllers\Controller;
use App\Models\Provincie\Provincie;
use App\Models\Zone\Zone;
use App\Models\Zone\ZoneProvincia;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;

class ZoneController extends Controller
{
    use HasRoles;

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function view()
    {
        return view('modules.zone.list');
    }

    public function getAll()
    {
        return response()->json(['status'=>TRUE,'data'=>Zone::where('status',1)->with('provincies.provincie')->get()]);
    }

    public function getProvincies()
    {
        return response()->json(['status'=>TRUE,'data'=>Provincie::select('id','name')->where('status',1)->get()]);
    }

    public function create(Request $request)
    {
        $name = $request->name;
        $provincie = $request->provincie;

        $zone = new Zone();
        $zone->name = $name;
        $zone->save();

        foreach ($provincie as $value) {
            $relationZoneProvincia = new ZoneProvincia();
            $relationZoneProvincia->zona_id = $zone->id;
            $relationZoneProvincia->provincie_id = $value;
            $relationZoneProvincia->save();
        }

        return response()->json(['status'=>TRUE,'data'=>Zone::where('id',$zone->id)->with('provincies.provincie')->first()]);
    }

    public function update(Request $request)
    {
        $name = $request->name;
        $provincie = $request->provincie;
        $idZone = $request->idZone;

        $zone = Zone::where('id',$idZone)->first();

        if(!$zone){
            return response()->json(['status'=>FALSE,'message'=>'Ocurrio un error encontrado la zona solicitada']);
        }

        $zone->name = $name;
        $zone->save();
        
        ZoneProvincia::where('zona_id',$zone->id)->delete();
        foreach ($provincie as $value) {
            $relationZoneProvincia = new ZoneProvincia();
            $relationZoneProvincia->zona_id = $zone->id;
            $relationZoneProvincia->provincie_id = $value;
            $relationZoneProvincia->save();
        }

        return response()->json(['status'=>TRUE,'data'=>Zone::where('id',$zone->id)->with('provincies.provincie')->first()]);
    }

    public function delete($id)
    {
        $zone = Zone::where('id',$id)->first();

        if(!$zone){
            return response()->json(['status'=>FALSE,'message'=>'La zona no se encontró']);
        }

        $zone->status = 0;
        $zone->save();

        return response()->json(['status'=>TRUE]);
    }
}
