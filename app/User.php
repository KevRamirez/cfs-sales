<?php

namespace App;

use App\Models\Fondelab\FondelabUserinfo;
use App\Models\Fondelab\FondelabUserRequest;
use App\Models\Main\MainCompany;
use App\Models\Main\MainCompanyDepartment;
use App\Models\Main\MainTurn;
use App\Models\Main\MainUniformType;
use App\Models\Studio\Studio;
use App\Models\Studio\StudioDepartment;
use App\Models\Studio\StudioLocation;
use App\Models\User\UserAssignedHardware;
use App\Models\User\UserAssignedUniform;
use App\Models\User\UserEducation;
use App\Models\User\UserEmergencyContact;
use App\Models\User\UserHealthCondition;
use App\Models\User\UserVehicle;
use App\Models\Zone\Zone;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function configRole()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

   public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id');
    }
}
