<?php

namespace App\Imports;

use App\Models\Clients\Client;
use App\Models\Diary\Diary;
use App\Models\Product\Product;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;

class DiaryImport implements ToCollection, SkipsEmptyRows
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //dd($collection);

        foreach($collection as $key => $value)
        {
            if($key == 0) { continue; }
            $fecha = '';
            $codeProduct = $value[0];
            $codeClient = $value[1];
            $timestamp = strtotime($value[2]);
            $date = Carbon::createFromFormat('d/m/Y', $value[2])->format('Y-m-d');
            $fecha = $date;
            $status = $value[3];
            $numberPurchases = $value[4];
            //var_dump($fecha);
            $statusValue = $status == '+' ? 1 : 0;

            $product = Product::where('code',$codeProduct)->first();

            $client = Client::where('code',$codeClient)->first();

            $dairy = new Diary();
            if($product){
                $dairy->product_id =  $product->id;
                $dairy->price_product = $product->price_box;
                $dairy->price_total = (double) $product->price_box * (int) $numberPurchases;
                $dairy->date = $fecha;

                if($statusValue){
                    $product->stock = $product->stock - $numberPurchases;
                }else{
                    $product->stock = $product->stock + $numberPurchases;
                }
                $product->save();
            }

            if($client){
                $dairy->client_id =  $client->id;
            }

            $dairy->status = $statusValue;
            $dairy->count_product = (int) $numberPurchases;

            $dairy->save();
        }
    }
}
