/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');
 import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
 Vue.use(BootstrapVue)
 
 import vSelect from 'vue-select'
 Vue.component('v-select', vSelect)
 
 import VueLazyload from 'vue-lazyload'
 Vue.use(VueLazyload)
 
 import VueUploadMultipleImage from 'vue-upload-multiple-image'
 Vue.component('vue-upload-multiple-image', VueUploadMultipleImage)
 
 import VueSweetalert2 from 'vue-sweetalert2'
 Vue.use(VueSweetalert2)
 
 import {utils} from './utils'
 Vue.prototype.$utils = utils
 
 import VueToastr2 from 'vue-toastr-2'
 window.toastr = require('toastr')
 Vue.use(VueToastr2)
 
 toastr.options = {
     "closeButton": false,
     "debug": false,
     "newestOnTop": false,
     "progressBar": true,
     "positionClass": "toast-top-right",
     "preventDuplicates": false,
     "onclick": null,
     "showDuration": "300",
     "hideDuration": "1000",
     "timeOut": "3000",
     "extendedTimeOut": "1000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "slideDown",
     "hideMethod": "fadeOut",
     "defaultStyle" : { "background-color": 'blue' },
 }
 
 import VueCurrencyInput from 'vue-currency-input'
 Vue.use(VueCurrencyInput, {
     globalOptions: {
         currency: null, // only override the default currency 'EUR' with 'USD'
         locale: 'de-DE',
         valueAsInteger: true,
         distractionFree: false,
         autoDecimalMode: false,
         valueRange: { min: 0 },
         allowNegative: false
     },
 })
 
 import AntDesign from "ant-design-vue";
 Vue.use(AntDesign);
 
 import VCalendar from 'v-calendar';
 Vue.use(VCalendar);
 
 import VueChatScroll from 'vue-chat-scroll';
 Vue.use(VueChatScroll);
 
 import VueFormWizard from 'vue-form-wizard'
 import 'vue-form-wizard/dist/vue-form-wizard.min.css'
 Vue.use(VueFormWizard)
 
 import VueClipboard from 'vue-clipboard2'
 VueClipboard.config.autoSetContainer = true;
 Vue.use(VueClipboard)
 
 
 import vueMoment from 'vue-moment';
 import moment from "moment";
 moment.locale("es");
 Vue.use(vueMoment,{
     moment
 });
 
 import VueAwesomeSwiper from 'vue-awesome-swiper'
 import 'swiper/css/swiper.css'
 Vue.use(VueAwesomeSwiper, /* { default options with global component } */)
 
 import vue2Dropzone from 'vue2-dropzone'
 Vue.component('vueDropzone', vue2Dropzone)
 
 
 import ToggleButton from 'vue-js-toggle-button'
 Vue.use(ToggleButton)
 
 import Lightbox from 'vue-easy-lightbox'
 Vue.use(Lightbox)
 
 import StarRating from 'vue-star-rating'
 Vue.component('star-rating', StarRating);
 
 import VueApexCharts from 'vue-apexcharts'
 Vue.use(VueApexCharts)
 
 Vue.component('apexchart', VueApexCharts)
 
 import VueEasyLightbox from 'vue-easy-lightbox';
 Vue.component('vue-easy-lightbox', VueEasyLightbox)
 
 import VueTypeaheadBootstrap from 'vue-typeahead-bootstrap';
 Vue.component('vue-typeahead-bootstrap', VueTypeaheadBootstrap)
 
 // Mixins
 import MixinUtils from './mixins/MixinUtils.js';
 Vue.mixin(MixinUtils);
 
 Vue.mixin({
     methods: {
         route: route
     }
 });
 
 export const bus = new Vue()
 window.bus = bus;
 
 import ZoomOnHover from "vue-zoom-on-hover";
 Vue.use(ZoomOnHover);
 
 import Vuex from 'vuex';
 import Vueditor from 'vueditor';
 import 'vueditor/dist/style/vueditor.min.css';
 /*'removeFormat', 'undo', '|', 'elements', 'fontName', 'fontSize', 'foreColor', 'backColor', 'divider',
     'bold', 'italic', 'underline', 'strikeThrough', 'links', 'divider', 'subscript', 'superscript',
     'divider', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', '|', 'indent', 'outdent',
     'insertOrderedList', 'insertUnorderedList'*/
 let config = {
     toolbar: [
         'bold', 'backColor', 'link'
     ],
     fontName: [
         {val: 'arial black'},
         {val: 'times new roman'},
         {val: 'Courier New'}
     ],
     fontSize: [
         '12px', '14px'
     ],
     uploadUrl: ''
 };
 Vue.use(Vuex);
 Vue.use(Vueditor, config);
 
 import Vue2Filters from 'vue2-filters'
 Vue.use(Vue2Filters)
 
 /**
  * The following block of code may be used to automatically register your
  * Vue components. It will recursively scan this directory for the Vue
  * components and automatically register them with their "basename".
  *
  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
  */
 
 const files = require.context('./', true, /\.vue$/i)
 files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default)) 
 
 //Vue.component('example-component', require('./components/ExampleComponent.vue').default);
 
 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 
 import store from './vuex/store';
 
 const app = new Vue({
     el: '#app',
     store,
     components: {
         VueUploadMultipleImage, 
     },
 });
 