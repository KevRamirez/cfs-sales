export default {
    name: "MixinCan",
    data() {
      return {
          DOMAIN: window.location.origin + '/',
      }
    },
    methods: {
        $can(permissionName) {
            return Permissions.indexOf(permissionName) !== -1;
        },
        $asset(file) {
            return this.DOMAIN + file;
        },
    },
};
