import Vue from 'vue';
import Vuex from "vuex";
import userStore from './user/user';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        message: 'hola desde vuex!',
    },
    mutations: {},
    actions: {

    },
    getters: {
        message: state => state.message,
    },
    modules:{
        user: userStore
    }
})
