'use strict';

export default {
    namespaced: true,
    state: {
        userInfo: {},
    },
    mutations: {
        addUserInfo (state, data){
            state.userInfo = data.info;
           // state.userInfo = [data, ...state.userInfo];
        }
    },
    actions: {
        GET_USER(context, id) {

            axios.get(route('user.getUserInfo', id)).then((res) => {
                context.commit('addUserInfo', res.data);
            });
        }
    },
    getters: {
        userInfo: state => state.userInfo,
    }
}
