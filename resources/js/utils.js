import moment from "moment";

export const utils = {
    validateForm: (errors) => {
        $(":input").removeClass('is-invalid');
        $(".invalid-feedback").remove();

        $.each(errors, function (name, error) {
            $(":input[name='" + name + "']").addClass('is-invalid');
            $(":input[name='" + name + "']:not(:radio)").after('<span class="invalid-feedback">' + error + '</span>');

            $("div[name='" + name + "']").addClass('is-invalid');
            $("div[name='" + name + "']").after('<span class="invalid-feedback d-block">' + error + '</span>');
        });
    },
    formatToDollars: (value) => {
        return new Intl.NumberFormat("us-US").format(value);
    },
    formatToPesos: (value) => {
        return new Intl.NumberFormat("de-DE").format(value);
    },
    openLink: (url, target) => {
        let tab = window.open(url, target);
    },
    dateCheck(from, to, check) {
        let d1 = from.split("-");
        let d2 = to.split("-");
        let c = check.split("-");

        let date_from = new Date(d1[0], parseInt(d1[1]) - 1, d1[2]);  // -1 because months are from 0 to 11
        let date_to = new Date(d2[0], parseInt(d2[1]) - 1, d2[2]);
        let date_check = new Date(c[0], parseInt(c[1]) - 1, c[2]);

        return date_check >= date_from && date_check <= date_to
    },
    dateIsOlder(to, check) {
        let d2 = to.split("-");
        let c = check.split("-");

        //let date_from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
        let date_to = new Date(d2[0], parseInt(d2[1]) - 1, d2[2]);
        let date_check = new Date(c[0], parseInt(c[1]) - 1, c[2]);

        return date_check < date_to
    },
    getNextDay(dayName) {
        // The current day
        let date = new Date();
        let now = date.getDay();

        // Days of the week
        let days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

        // The index for the day you want
        let day = days.indexOf(dayName.toLowerCase());

        // Find the difference between the current day and the one you want
        // If it's the same day as today (or a negative number), jump to the next week
        let diff = day - now;
        diff = diff < 1 ? 7 + diff : diff;

        // Get the timestamp for the desired day
        let nextDayTimestamp = date.getTime() + (1000 * 60 * 60 * 24 * diff);

        // Get the next day
        return new Date(nextDayTimestamp);
    },
    getPreviousMonday(date = null) {
        let prevMonday = date && new Date(date.valueOf()) || new Date();
        prevMonday.setDate(prevMonday.getDate() - (prevMonday.getDay() + 6) % 7);

        return prevMonday;
    },
    collapseMenuUtils() {
        $('body,html').removeClass('menu-expanded');
        $('body,html').addClass('menu-collapsed');
    },
    deleteAccentuation(text) {
        let from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
            to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
            mapping = {};
        let ret = [];

        for (var i = 0, j = from.length; i < j; i++)
            mapping[from.charAt(i)] = to.charAt(i);

        for (let i = 0, j = text.length; i < j; i++) {
            const c = text.charAt(i);

            if (mapping.hasOwnProperty(text.charAt(i))) {
                ret.push(mapping[c]);
            } else {
                ret.push(c);
            }
        }

        return ret.join('');

    },
    generatePasswordPlataform() {

        const characterValid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        const characterNumber = "0123456789";
        const characterUppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const characterLowercase = "abcdefghijklmnopqrstuvwxyz";

        let passwordInitial = characterUppercase.charAt(Math.floor(Math.random() * characterUppercase.length));
        let password = passwordInitial;

        for (let i = 1, n = characterValid.length; i <= 9; i++) {

            let charSelected = characterValid.charAt(Math.floor(Math.random() * n));

            const isNumber = parseInt(charSelected);
            const isLowercase = characterLowercase.search(charSelected);

            if (i == 4 && isLowercase == -1) {
                charSelected = characterLowercase.charAt(Math.floor(Math.random() * characterLowercase.length));
            }

            if (i == 8 && !isNumber) {
                charSelected = characterNumber.charAt(Math.floor(Math.random() * characterNumber.length));
            }

            password += charSelected;
        }

        return password;

    },
    getToday(){
        return moment().toDate();
    }
}
