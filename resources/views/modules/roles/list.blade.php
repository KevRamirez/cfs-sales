@extends('layouts.app')

@section('title', 'Roles')

@section('content')
    <roles :roles="{{ $roles }}"></roles>
@endsection
