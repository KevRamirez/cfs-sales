@extends('layouts.app')

@section('title', 'Permisos')

@section('content')
    <permissions :roles="{{ $roles }}" :modules="{{ $modules }}"></permissions>
@endsection
