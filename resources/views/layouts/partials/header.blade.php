<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-dark navbar-shadow bg-white">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-lg-none mr-auto"><a style="color: #009688;" class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="/home">
                        <img class="brand-logo" src="{{ asset('images/logo/logocfs.png') }}" alt="Cigar Family Spain" style="max-width:70px;">
                    </a>
                </li>
                <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><!--<i class="toggle-icon ft-toggle-right font-medium-3" style="color:#2b2b2b;" data-ticon="ft-toggle-right"></i>--></a></li>
                <li class="nav-item d-lg-none"><a style="color: #009688;" class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand" style="color:#2b2b2b;" href="#"><i class="ficon ft-maximize"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            @auth
                                <span class="mr-1 user-name text-bold-700" style="color:#2b2b2b;">
                                    {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                                </span>

                                <span class="avatar avatar-online">
                                @if(!is_null(auth()->user()->avatar))
                                        <img src="{{ asset('storage/users/' . auth()->user()->id . '/avatar/' . auth()->user()->avatar) }}" alt="avatar">
                                        <i></i>
                                    @else
                                        <img src="{{ asset('app-assets/images/no-avatar.png') }}" alt="avatar">
                                        <i></i>
                                    @endif
                                </span>
                            @endauth
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <!--<a class="dropdown-item" href="{{ route('user.profile')  }}">
                                <i class="ft-user" ></i> Mi Perfil
                            </a>
                            <div class="dropdown-divider"></div>-->
                            <a class="dropdown-item" href="{{ route('logout') }}">
                                <i class="ft-power"></i> Cerrar Sesión
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->
