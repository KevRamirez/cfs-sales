<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-twl-cool-blue menu-accordion menu-shadow" data-scroll-to-active="true" style="touch-action: none; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item {{ request()->is('home') ? 'active' : '' }}">
                <a href="{{route('home')}}">
                    <i class="la la-dashboard"></i>
                    <span class="menu-title" data-i18n="eCommerce">
                        Escritorio
                    </span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('reports') ? 'active' : '' }}">
                <a href="{{route('reports.view')}}">
                    <i class="la la-bar-chart"></i>
                    <span class="menu-title" data-i18n="eCommerce">
                        Informes
                    </span>
                </a>
            </li>

              <li class="nav-item has-sub">
                  <a href="#">
                      <i class="la la-users"></i>
                        <span class="menu-title" data-i18n="Usuarios">Usuarios</span>
                    </a>
                <ul class="menu-content" style="">
                    <li class="nav-item {{ request()->is('client') ? 'active' : '' }}">
                        <a href="{{route('client.view')}}">
                            <i class="fas fa-user-tie"></i>
                            <span class="menu-title" data-i18n="eCommerce">
                                Clientes
                            </span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is('users') ? 'active' : '' }}">
                        <a href="{{route('users.view')}}">
                            <i class="fas fa-users"></i>
                            <span class="menu-title" data-i18n="eCommerce">
                                Equipo
                            </span>
                        </a>
                    </li>
                </ul>
              </li>

              <li class="nav-item has-sub">
                <a href="#"><i class="la la-gears"></i>
                    <span class="menu-title" data-i18n="Configuracion">Configuración</span>
                </a>
                <ul class="menu-content" style="">
                    <li class="nav-item {{ request()->is('family') ? 'active' : '' }}">
                        <a href="{{route('family.view')}}">
                            <i class="fas fa-smoking"></i>
                            <span class="menu-title" data-i18n="eCommerce">
                                Familias
                            </span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is('product') ? 'active' : '' }}">
                        <a href="{{route('product.view')}}">
                            <i class="fad fa-shopping-cart"></i>
                            <span class="menu-title" data-i18n="eCommerce">
                                Productos
                            </span>
                        </a>
                    </li>
                    <li class="has-sub">
                        <a class="menu-item" href="#">
                            <i class="la la-street-view"></i>
                            <span data-i18n="Ubicaciones">Ubicaciones</span></a>
                        <ul class="menu-content">
                            <li class="nav-item {{ request()->is('provincie') ? 'active' : '' }}">
                                <a href="{{route('provincie.view')}}">
                                    <i class="fad fa-map-marker-check"></i>
                                    <span class="menu-title" data-i18n="eCommerce">
                                        Provincia
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('zone') ? 'active' : '' }}">
                                <a href="{{route('zone.view')}}">
                                    <i class="fad fa-map-marked-alt"></i>
                                    <span class="menu-title" data-i18n="eCommerce">
                                        Zonas
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item {{ request()->is('community') ? 'active' : '' }}">
                                <a href="{{route('community.view')}}">
                                    <i class="fas fa-map-pin"></i>
                                    <span class="menu-title" data-i18n="eCommerce">
                                        Comunidades
                                    </span>
                                </a>
                            </li>
                        </ul>
                      </li>
                </ul>
              </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->
